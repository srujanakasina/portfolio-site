-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Host: 10.123.0.91:3306
-- Generation Time: Mar 30, 2015 at 10:00 PM
-- Server version: 5.5.38
-- PHP Version: 5.4.39-0+deb7u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `protc93_testlog`
--

-- --------------------------------------------------------

--
-- Table structure for table `userInfo`
--

CREATE TABLE IF NOT EXISTS `userInfo` (
  `count` int(11) NOT NULL,
  `lastName` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `studentInstructor` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `major` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `participateStatus` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `futureGoals` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userInfo`
--

INSERT INTO `userInfo` (`count`, `lastName`, `firstName`, `username`, `password`, `email`, `studentInstructor`, `major`, `website`, `participateStatus`, `futureGoals`, `bio`) VALUES
(3, 'Cory', 'Travis', '', '', 'ttcory1@dmacc.edu', 'student', '', '', 'no', '', ''),
(2, 'Fish', 'Dork', 'theUser', 'thePass', 'dorkfish@corndog.com', 'student', 'Web Development', '', 'yes', 'Answer one.', 'Answer two.'),
(5, 'Beach', 'Turtle', '', '', 'official.over9000turtles@gmail.com', 'instructor', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `userInfo`
--
ALTER TABLE `userInfo`
  ADD PRIMARY KEY (`count`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `userInfo`
--
ALTER TABLE `userInfo`
  MODIFY `count` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
